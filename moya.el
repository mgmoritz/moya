;;; moya.el --- Yaml operations

;; Copyright (C) 2011 Free Software Foundation, Inc.

;; Author: Marcos G Moritz <marcosmoritz@gmail.com>
;; Version: 0.0.1
;; Package-Requires: ()
;; Keywords: yaml
;; URL: https://mo-profile.com

;;; Commentary:
;; This packages handles operations over yaml files

;; This package handles changes over yaml files cycling
;; through values in a given level.
;; The specific operation implemented allows the base64
;; encoding/decoding of all values under data key

(defun moya--level-op (level-regexp op)
  "Apply operation to all items in the level after the regexp"
  (save-excursion
    (goto-char (point-min))
    (search-forward-regexp level-regexp)
    (back-to-indentation)
    (let ((data-column (current-column)))
      (forward-line 1)
      (back-to-indentation)
      (while (> (current-column) data-column)
        (search-forward ":")
        (skip-chars-forward "[:space:]")
        (let ((value-start (point)))
          (end-of-line)
          (apply op `(,value-start ,(point))))
        (forward-line 1)
        (back-to-indentation)))))

(defun moya-level-base64-encode ()
  "Encode data values in yaml"
  (interactive)
  (moya--level-op "^[[:space:]]*data:$" 'base64-encode-region))

(defun moya-level-base64-decode ()
  "Encode data values in yaml"
  (interactive)
  (moya--level-op "^[[:space:]]*data:$" 'base64-decode-region))

(provide 'moya)
