# moya

This packages handles operations over yaml files

This package handles changes over yaml files cycling
through values in a given level.

The specific operation implemented allows the base64
encoding/decoding of all values under `data` key
